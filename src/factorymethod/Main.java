/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorymethod;

/**
 *
 * @author aluno.redes
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        FactoryCar genericFactory = new ConcreteFactoryCar();//tem o tipo básico assim porque o tipo da instância porderia ser de alguma fábrica específica, exemplo: Factorycar"fábrica"  concreteFactoryCar"toyota"
        
        Car car1 = genericFactory.makeCar(CarModels.gol);
        Car car2 = genericFactory.makeCar(CarModels.uno);
        Car car3 = genericFactory.makeCar(CarModels.corolla);
        
        car1.showInfo();
        car2.showInfo();
        car3.showInfo();
        
        FactoryCar toyota = new ToyotaFactory();
        
        Car car4 = toyota.makeCar(CarModels.corolla);
        Car car5 = toyota.makeCar(CarModels.gol);
        
        car4.showInfo();
        //car5.showInfo(); //deste jeito pode dar erro, por isso pode-se usar o if else para evitar

        if (car5 != null) {
            car5.showInfo();
        } else {
            System.out.println("can´t create Car");
        }
    }
    
}
